module gitea.com/jolheiser/whatthecommit

go 1.13

require (
	gitea.com/jolheiser/beaver v1.0.0
	github.com/urfave/cli/v2 v2.1.1
)
