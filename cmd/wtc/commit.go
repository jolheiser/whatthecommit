package main

import (
	"fmt"
	"os/exec"

	"github.com/urfave/cli/v2"

	"gitea.com/jolheiser/beaver"
	"gitea.com/jolheiser/whatthecommit"
)

var (
	Commit = &cli.Command{
		Name:    "commit",
		Aliases: []string{"c"},
		Usage:   "Show a random commit message",
		Action:  runCommit,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "yolo",
				Aliases:     []string{"y"},
				Usage:       "YOLO, just do the commit",
				Value:       false,
				Destination: &yoloFlag,
			},
		},
	}
	yoloFlag bool
)

func runCommit(ctx *cli.Context) error {
	commit := whatthecommit.Commit()
	if !yoloFlag {
		beaver.Info(commit)
		return nil
	}

	commitCmd := exec.Command("git", "commit", "-a", "-m", commit)
	out, err := commitCmd.Output()
	if err != nil {
		return fmt.Errorf("%s: %v", out, err)
	}
	return nil
}
