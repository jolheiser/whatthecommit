package main

import (
	"os"

	"github.com/urfave/cli/v2"

	"gitea.com/jolheiser/beaver"
	"gitea.com/jolheiser/whatthecommit"
)

var (
	Version = "development"
)

func main() {
	app := &cli.App{
		Name:    "What The Commit",
		Usage:   "Generate random commit messages",
		Version: Version,
		Action: func(ctx *cli.Context) error {
			beaver.Info(whatthecommit.Commit())
			return cli.ShowAppHelp(ctx)
		},
		Commands: []*cli.Command{
			Commit,
			Yolo,
		},
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Error(err)
	}
}
