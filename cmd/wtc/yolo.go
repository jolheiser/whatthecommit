package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"

	"gitea.com/jolheiser/beaver"
)

var (
	Yolo = &cli.Command{
		Name:    "yolo",
		Aliases: []string{"y"},
		Usage:   "Install/Remove the YOLO alias for Git",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "global",
				Aliases:     []string{"g"},
				Usage:       "Whether to install YOLO globally",
				Value:       false,
				Destination: &globalFlag,
			},
		},
		Action: runYolo,
	}
	globalFlag bool
)

func runYolo(ctx *cli.Context) error {
	level := "--local"
	if globalFlag {
		level = "--global"
	}

	checkCmd := exec.Command("git", "config", level, "alias.yolo")
	out, _ := checkCmd.Output()

	exists := strings.TrimSpace(string(out)) != ""

	var configCmd *exec.Cmd
	if exists {
		beaver.Info("Removing yolo alias from", level, "git config")
		configCmd = exec.Command("git", "config", level, "--unset", "alias.yolo")
	} else {
		ex, err := os.Executable()
		ex = filepath.ToSlash(ex)
		if err != nil {
			return fmt.Errorf("could not get path to WTC binary: %v", err)
		}
		beaver.Info("Adding yolo alias to", level, "git config")
		configCmd = exec.Command("git", "config", level, "alias.yolo", fmt.Sprintf("!%s commit --yolo", ex))
	}

	out, err := configCmd.Output()
	if err != nil {
		return fmt.Errorf("%s: %v", out, err)
	}
	return nil
}
