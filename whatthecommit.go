//go:generate go run generate.go
//go:generate go fmt ./...

package whatthecommit

import (
	"math/rand"
	"time"
)

var (
	commits []string
)

// Commit returns a random commit from commits
func Commit() string {
	rand.Seed(time.Now().Unix())
	return commits[rand.Intn(len(commits))]
}
