GO ?= go

VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')
LDFLAGS := $(LDFLAGS) -X "main.Version=$(VERSION)"

.PHONY: build
build: generate
	$(GO) build -ldflags '-s -w $(LDFLAGS)' ./cmd/wtc/

.PHONY: generate
generate:
	$(GO) generate ./...

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: lint
lint:
	@hash golangci-lint > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		export BINARY="golangci-lint"; \
		curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.23.1; \
	fi
	golangci-lint run --timeout 5m
