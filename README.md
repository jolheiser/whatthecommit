# whatthecommit

A Go library and CLI inspired by [whatthecommit](http://whatthecommit.com/)

## Building

### Go
To build purely with Go
```
go build ./cmd/wtc/
```

### Makefile
To do a full build, including version and updating commits file
```
make build
```

### Updating commits file
```
go generate ./...
```

## Usage
```
wtc help
```